DROP USER csa;
--CREATE USER csa IDENTIFIED BY "1";
--GRANT CONNECT  TO csa;
--RESOURCE, DBA
--grant connect to csa identified by "1";
grant all privileges to csa identified by "1";
grant all privileges to erib_test identified by "1";
grant all privileges to erib_test2 identified by "1";

--GRANT CREATE SESSION TO csa;
--GRANT ALL PRIVILEGES TO csa;
GRANT UNLIMITED TABLESPACE TO csa;
--alter user helper quota unlimited on SYSTEM;
GRANT UNLIMITED TABLESPACE TO csa;

create user test no authentication;

--grant create table to helper;
--grant create view to helper;
--grant create sequence to helper;
--grant create procedure to helper;




